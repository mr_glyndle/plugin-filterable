<?php
include( plugin_dir_path( __FILE__ ) . '/map-script.php');
$map = get_field('map');
if( !empty($map) ) { ?>

	<div class="gmap"><div class="marker" data-lat="<?php echo $map['lat']; ?>" data-lng="<?php echo $map['lng']; ?>"></div></div>

<?php } ?>
