<?php get_header(); $site_width = get_field('site_width', 'option');?>

	<section id="accommodation" class="category accom <?php if ($site_width == 'full') { echo " full"; };?> clearfix">

		<?php

		// set variables
		$term_id = get_queried_object_id();
		$tax = 'filter_by';

		// Load filters
		include( plugin_dir_path( __FILE__ ) . 'filter/filters.php');


		if($term_id == '') {
			$term = get_terms( array(
			    'taxonomy' 		=> 	$tax,
				'parent' 		=> 	0,
				'number'		=> 	1,
				'hide_empty'	=>	'true'
			) );
			$term = $term[0];
			$term_id = $term->term_id;
		}

		$term_children = get_term_children($term_id, $tax);

		// Get ID(s) of current tags
		$filter_tag_ids = array();
		if(!empty($current_tags[0])) {
			foreach ($current_tags as $current_tag) {
				$filter_tag_terms[] = get_term_by( 'slug', $current_tag, 'must_have');
			}
			foreach ($filter_tag_terms as $filter_tag_term) {
				$filter_tag_ids[] = $filter_tag_term->term_id;
			}
		};



		// ADD MAP - if selected to be displayed
		$include_map = get_field('include_map', 'filter_by_' . $term_id);
		if ( isset($include_map) && $include_map !== '' && $include_map !== 'no' ) {
			include( plugin_dir_path( __FILE__ ) . '/map/multi-map.php');
		}

		// if category has child categories
		if (get_term_children($term_id, $tax) != null) {

			// get child terms
			$term_children = get_terms( $tax, array( 'child_of' => $term_id, ) );
			$count = 999;
			$newterms = array();

			foreach ($term_children as $term_child) {
				$order = "";
				$order = get_field('order', $term_child); //THIS CUSTOM FIELD VALUE
				if ($order == '') {
					$order=$count;
				}
				$newterms[$order] = $term_child->term_id;
				++$count;
			}

			// Sort them numerically:
			ksort( $newterms, SORT_NUMERIC );

			// loop through them to get posts and category info
			foreach ($newterms as $term_child) {

				// Get posts from current category and tags
				if(!empty($filter_tag_ids)) {
					$args = array(
						'post_type' => 'filterable',
						'orderby'	=>	'title',
						'order'	=>	'ASC',
						'posts_per_page'   => -1,
						'tax_query' => array(
							'relation' => 'AND',
							array(
								'taxonomy' => 'filter_by',
								'field'    => 'term_id',
								'terms'    => $term_child,
								'include_children' => false,
							),
							array(
								'taxonomy'	=> 'must_have',
								'field'		=> 'term_id',
								'terms'		=> $filter_tag_ids,
								'operator' => 'AND',
								'include_children' => false,
							),
						),
					);

				// Get posts from current category
				} else {
					$args = array(
						'post_type' => 'filterable',
						'orderby'	=>	'title',
						'order'	=>	'ASC',
						'posts_per_page'   => -1,
						'tax_query' => array(
							array(
								'taxonomy' => 'filter_by',
								'field'    => 'term_id',
								'terms'    => $term_child,
								'include_children' => false
							),
						),
					);
				}


				$query = new WP_Query( $args );

				if ( $query->have_posts() ) {

					$this_cat = get_term( $term_child, 'filter_by' ); ?>

					<div class="sub_cat">
						<div class="txt_blk normal">
							<h2>
								<a href="<?php echo get_term_link($this_cat->term_id) ?>" title="<?php echo $this_cat->name; ?>" alt="<?php echo $this_cat->name; ?>">
									<?php echo $this_cat->name; ?>
								</a>
							</h2>
							<?php if ($this_cat->description !== '') {
								$cat_desc = wpautop( $this_cat->description );
								echo $cat_desc;
							}; ?>
						</div>
						<div class="grid_cont">
							<div class="grid card <?php if ($site_width == 'full') { echo " full "; }; echo 'ti_' . $query->post_count; if ($query->post_count % 2 == 0) { echo ' even'; } else { echo ' odd'; };?>">
								<div class="items">

									<?php $grid_item_count = 1;
									while ( $query->have_posts() ) : $query->the_post();

										include(plugin_dir_path( __FILE__ ) . 'filter/card_view.php');

									endwhile;
									wp_reset_query(); ?>
								</div>
							</div>
						</div>
					</div>
				<?php }
			}

	// If category has no child categories
	} else {
		$term_child = get_term( $term_id, $tax );

		// Get posts from current category and tags
		if(!empty($filter_tag_ids)) {
			$args = array(
				'post_type' => 'filterable',
				'orderby'	=>	'title',
				'order'	=>	'ASC',
				'relation' => 'AND',
				'posts_per_page'   => -1,
				'tax_query' => array(
					'relation' => 'AND',
					array(
						'taxonomy' => 'filter_by',
						'field'    => 'term_id',
						'terms'    => $term_child->term_id,
						'include_children' => false
					),
					array(
						'taxonomy' => 'must_have',
						'field'    => 'term_id',
						'terms'    => $filter_tag_ids,
						'operator' => 'AND',
						'include_children' => false,
					),
				),
			);
		// Get posts from current category
		} else {
			$args = array(
				'post_type' => 'filterable',
				'orderby'	=>	'title',
				'order'	=>	'ASC',
				'posts_per_page'   => -1,
				'tax_query' => array(
					array(
						'taxonomy' => 'filter_by',
						'field'    => 'term_id',
						'terms'    => $term_child->term_id,
						'include_children' => false
					),
				),
			);
		}
		$query = new WP_Query( $args );
		$count = $query->post_count;
		if ( $query->have_posts() ) {
			$this_cat = get_term( $term_child, 'filter_by' ); ?>

			<div class="sub_cat grid_cont">

				<div class="grid card <?php echo ' card '; if ($site_width == 'full') { echo " full "; }; echo 'ti_' . $query->post_count; if ($query->post_count % 2 == 0) { echo ' even'; } else { echo ' odd'; };?>">
					<div class="items">

						<?php $grid_item_count = 1;
						while ( $query->have_posts() ) : $query->the_post();

							include(plugin_dir_path( __FILE__ ) . 'filter/card_view.php');

						endwhile;
						wp_reset_query(); ?>
					</div>
				</div>
			</div>
		<?php }
 } ?>
</section><?php get_footer(); ?>
