<?php get_header();

if (have_posts()) {

	while (have_posts()) : the_post();

	// Is there a map for this item?
	$location = get_field('map');
	if (isset($location) && $location !== '') {
		include( plugin_dir_path( __FILE__ ) . '/map/single-map.php');
	}

	// check if the flexible content field has rows of data
    if( have_rows('cont') ) {

			$item_count = 1;

			// loop through the rows of data
			while ( have_rows('cont') ) : the_row();

				// Make these reusable functions rather than repeatedly calling files?
				include(locate_template('partials/slice_loop.php'));

			endwhile;

		} else {

			echo '<div class="slice text standard"><div class="txt_blk normal s_over avs_default  avm_default clearfix"><div class="text_content">';
			echo the_content();
			echo '</div></div></div>';

	 	};

	endwhile;

} else { ?>

	<div class="slice text standard">
		<div class="txt_blk normal s_over avs_15  avm_default clearfix">
			<div class="text_content">
				<div class="alert alert-info">
					<h1>Sorry, we can't find the page you're looking for</h1>
					<p>Please use the navigation or, seach the site with the options in the menu.</p>
				</div>
			</div>
		</div>
	</div>

<?php };

if (comments_open() || get_comments_number()) { ?><div class="txt_blk comments"><?php comments_template(); ?></div><?php };

get_footer();
