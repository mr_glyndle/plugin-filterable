<?php
/*
 Plugin Name:      		Category Filter and map
 Plugin URI:       		https://bitbucket.org/thebrandchap/plugin-filterable/
 Description:       	View and filter category content by tags. Group tags. Add posts as location to display on a map. Requires Sitebooster Theme and ACF.
 Version:           	1.0.01
 Author:            	Glyn Harrison
 License:           	GNU General Public License v2
 License URI:       	http://www.gnu.org/licenses/gpl-2.0.html
 Bitbucket Plugin URI: 	https://bitbucket.org/thebrandchap/plugin-filterable/
 */

// Register Custom Post Type
function filter_post_type() {

	$labels = array(
		'name'                  => 'Filterable',
		'singular_name'         => 'Filterable',
		'menu_name'             => 'Filterable',
		'name_admin_bar'        => 'Filterable',
		'archives'              => 'Filterable Archives',
		'attributes'            => 'Filterable Attributes',
		'parent_item_colon'     => 'Parent Item:',
		'all_items'             => 'All Items',
		'add_new_item'          => 'Add New Item',
		'add_new'               => 'Add New',
		'new_item'              => 'New Item',
		'edit_item'             => 'Edit Item',
		'update_item'           => 'Update Item',
		'view_item'             => 'View Item',
		'view_items'            => 'View Items',
		'search_items'          => 'Search Item',
		'not_found'             => 'Not found',
		'not_found_in_trash'    => 'Not found in Trash',
		'featured_image'        => 'Feature Image',
		'set_featured_image'    => 'Set featured image',
		'remove_featured_image' => 'Remove featured image',
		'use_featured_image'    => 'Use as featured image',
		'insert_into_item'      => 'Insert into item',
		'uploaded_to_this_item' => 'Uploaded to this item',
		'items_list'            => 'Items list',
		'items_list_navigation' => 'Items list navigation',
		'filter_items_list'     => 'Filter items list',
	);
	$args = array(
		'label'                 => 'Filterable',
		'description'           => 'Filterable Products',
		'labels'                => $labels,
		'supports'              => array( 'title', 'excerpt', 'thumbnail', 'comments' ),
		'taxonomies'            => array( 'filterable_tax', 'must_have' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-randomize',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'filterable', $args );

}
add_action( 'init', 'filter_post_type', 0 );


// Register Filterable Categories
function filterable_tax() {

	$labels = array(
		'name'                       => 'Filter categories',
		'singular_name'              => 'Filter category',
		'menu_name'                  => 'Category',
		'all_items'                  => 'All categories',
		'parent_item'                => 'Parent category',
		'parent_item_colon'          => 'Parent category:',
		'new_item_name'              => 'New category name',
		'add_new_item'               => 'Add new category',
		'edit_item'                  => 'Edit category',
		'update_item'                => 'Update category',
		'view_item'                  => 'View category',
		'separate_items_with_commas' => 'Separate categories with commas',
		'add_or_remove_items'        => 'Add or remove categories',
		'choose_from_most_used'      => 'Choose from the most used',
		'popular_items'              => 'Popular categories',
		'search_items'               => 'Search categories',
		'not_found'                  => 'Category Not Found',
		'no_terms'                   => 'No category',
		'items_list'                 => 'Category list',
		'items_list_navigation'      => 'Category list navigation',
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_in_admin_bar'          => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
	);
	register_taxonomy( 'filter_by', array( 'filterable' ), $args );

}
add_action( 'init', 'filterable_tax', 0 );


// Register Filterable Tags
function must_haves() {

	$labels = array(
		'name'                       => 'Filter tags',
		'singular_name'              => 'Filter tag',
		'menu_name'                  => 'Tags',
		'all_items'                  => 'All tags',
		'parent_item'                => 'Parent tag',
		'parent_item_colon'          => 'Parent tag:',
		'new_item_name'              => 'New tag',
		'add_new_item'               => 'Add new tag',
		'edit_item'                  => 'Edit tag',
		'update_item'                => 'Update tag',
		'view_item'                  => 'View tag',
		'separate_items_with_commas' => 'Separate tags with commas',
		'add_or_remove_items'        => 'Add or remove tags',
		'choose_from_most_used'      => 'Choose from the most used',
		'popular_items'              => 'Popular tags',
		'search_items'               => 'Search tags',
		'not_found'                  => 'tag Not Found',
		'no_terms'                   => 'No tags',
		'items_list'                 => 'tags list',
		'items_list_navigation'      => 'tags list navigation',
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_in_admin_bar'          => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'must_have', array( 'filterable' ), $args );

}
add_action( 'init', 'must_haves', 0 );


// Use the plugin template files
add_filter( 'template_include', 'template_include_filterable', 1 );
function template_include_filterable( $template_path ) {

	if ( get_post_type() == 'filterable' ) {

		if ( is_single() ) {
			$template_path = plugin_dir_path( __FILE__ ) . 'single-filter.php';

			function filter_add_footer_styles() {
				$filename = get_stylesheet_directory_uri() . '/css/accommodation/accommodation_single.css';
				$root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
				$filenonroot = str_replace($root,"",$filename);
				if (file_exists($filenonroot)) {
					$filename = $filename . '?m=' . date ("YmdHi", filemtime($filenonroot));
				}
				wp_enqueue_style( 'filter-styles', $filename );
			};
			add_action( 'get_footer', 'filter_add_footer_styles' );

		} else if ( is_category() || is_archive() ) {

			$template_path = plugin_dir_path( __FILE__ ) . 'taxonomy-filter_type.php';

			function filter_add_footer_styles() {
				$filename = get_stylesheet_directory_uri() . '/css/accommodation/accommodation_tax.css';
				$root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
				$filenonroot = str_replace($root,"",$filename);
				if (file_exists($filenonroot)) {
					$filename = $filename . '?m=' . date ("YmdHi", filemtime($filenonroot));
				}
				wp_enqueue_style( 'filter-styles', $filename );
			};
			add_action( 'get_footer', 'filter_add_footer_styles' );

		}
	}
	return $template_path;
}



// Display a custom taxonomy dropdown in admin
add_action('restrict_manage_posts', 'filter_post_type_by_taxonomy');
function filter_post_type_by_taxonomy() {
	global $typenow;
	$post_type = 'filterable';
	$taxonomy  = 'filter_by';
	if ($typenow == $post_type) {
		$selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
		$info_taxonomy = get_taxonomy($taxonomy);
		wp_dropdown_categories(array(
			'show_option_all' => __("Show All {$info_taxonomy->label}"),
			'taxonomy'        => $taxonomy,
			'name'            => $taxonomy,
			'orderby'         => 'name',
			'selected'        => $selected,
			'show_count'      => true,
			'hide_empty'      => true,
		));
	};
}

// Filter posts by taxonomy in admin
add_filter('parse_query', 'convert_id_to_term_in_query');
function convert_id_to_term_in_query($query) {
	global $pagenow;
	$post_type = 'filterable'; // change to your post type
	$taxonomy  = 'filter_by'; // change to your taxonomy
	$q_vars    = &$query->query_vars;
	if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {
		$term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
		$q_vars[$taxonomy] = $term->slug;
	}
}


if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_5d0a0aa7c4563',
	'title' => 'Filter - Tags',
	'fields' => array(
		array(
			'key' => 'field_5d0a0aa7cf87c',
			'label' => 'Icon',
			'name' => 'af_ico',
			'type' => 'image',
			'instructions' => 'For best results, image should be a square png with a transparent background',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'thumbnail',
			'library' => 'all',
			'min_width' => 150,
			'min_height' => 150,
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
		array(
			'key' => 'field_5d0a0aa7cf889',
			'label' => 'Group by',
			'name' => 'group',
			'type' => 'text',
			'instructions' => 'Add a name here to sort the tags by. To group tags the names must match exactly',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'taxonomy',
				'operator' => '==',
				'value' => 'must_have',
			),
		),
	),
	'menu_order' => 32,
	'position' => 'acf_after_title',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

acf_add_local_field_group(array(
	'key' => 'group_5d0a0d00aa1fb',
	'title' => 'Filter - Category',
	'fields' => array(
		array(
			'key' => 'field_5d0a0d00b0227',
			'label' => 'Options',
			'name' => 'options',
			'type' => 'clone',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'clone' => array(
				0 => 'field_596dd8a04c538',
				1 => 'field_57064501f7ef9',
				2 => 'field_570649f2fb9bc',
			),
			'display' => 'seamless',
			'layout' => 'block',
			'prefix_label' => 0,
			'prefix_name' => 0,
		),
		array(
			'key' => 'field_5d0a0d00b0239',
			'label' => 'Order',
			'name' => 'order',
			'type' => 'number',
			'instructions' => 'Add a number here to sort the categories by: the lower the number the higher on the page eg. 1 = top of page. **TIP** If you foresee adding extra categories in the future use multiples of ten initially, then you have 9 spaces between each type to add additional types into.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'min' => '',
			'max' => '',
			'step' => 1,
		),
		array(
			'key' => 'field_5d133fafd038d',
			'label' => 'Include a map?',
			'name' => 'include_map',
			'type' => 'radio',
			'instructions' => 'If you have added location information to the items in this category you can have them display in a map at the top of this category page',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array(
				'yes' => 'Yes',
				'no' => 'No',
			),
			'allow_null' => 0,
			'other_choice' => 0,
			'default_value' => 'no',
			'layout' => 'horizontal',
			'return_format' => 'value',
			'save_other_choice' => 0,
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'taxonomy',
				'operator' => '==',
				'value' => 'filter_by',
			),
		),
	),
	'menu_order' => 33,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

acf_add_local_field_group(array(
	'key' => 'group_5d0a0d3a545a3',
	'title' => 'Filter - Items',
	'fields' => array(
		array(
			'key' => 'field_5d0a0d3a5af5c',
			'label' => 'Header Settings',
			'name' => 'head_set',
			'type' => 'clone',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'clone' => array(
				0 => 'group_57694dc34b52a',
			),
			'display' => 'group',
			'layout' => 'block',
			'prefix_label' => 0,
			'prefix_name' => 1,
		),
		array(
			'key' => 'field_5d13405261925',
			'label' => 'Does the item have a location?',
			'name' => 'location',
			'type' => 'true_false',
			'instructions' => 'If you\'d like to include a map to this item, add its location here.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '33',
				'class' => '',
				'id' => '',
			),
			'message' => '',
			'default_value' => 0,
			'ui' => 0,
			'ui_on_text' => '',
			'ui_off_text' => '',
		),
		array(
			'key' => 'field_5d13402361924',
			'label' => 'Location',
			'name' => 'map',
			'type' => 'google_map',
			'instructions' => 'Does the item have a location?',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5d13405261925',
						'operator' => '==',
						'value' => '1',
					),
				),
			),
			'wrapper' => array(
				'width' => '67',
				'class' => '',
				'id' => '',
			),
			'center_lat' => '53.472225',
			'center_lng' => '-2.2935017',
			'zoom' => 10,
			'height' => '',
		),
		array(
			'key' => 'field_5d0a0d8b34f3c',
			'label' => 'Slices',
			'name' => 'slices',
			'type' => 'clone',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'clone' => array(
				0 => 'group_5ab28d0b3cfd3',
			),
			'display' => 'seamless',
			'layout' => 'block',
			'prefix_label' => 0,
			'prefix_name' => 0,
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'filterable',
			),
		),
	),
	'menu_order' => 34,
	'position' => 'acf_after_title',
	'style' => 'seamless',
	'label_placement' => 'top',
	'instruction_placement' => 'field',
	'hide_on_screen' => array(
		0 => 'the_content',
	),
	'active' => true,
	'description' => '',
));

endif;
