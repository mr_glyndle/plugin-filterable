<?php
$cat_title = single_cat_title('', false);
$cat_title = 'Currently Viewing: ' . $cat_title;
?>
<nav class="accom filters">
	<div class="selected"><a class="inline-block vmiddle lable">Current choices: <?php echo '<a class="reset" href="'. site_url() . '/filter/">Clear filters</a>';?>
		<ul class="inline-block vtop menu">
			<li class="menu-item-has-children dropdown"><a class="btn cat" href="#category">Type: <?php single_cat_title(); ?></a>
				<ul class="sub-menu accom cats">
					<?php $args = array(
						'child_of'				=> 0,
						'current_category'		=> 0,
						'depth'					=> 0,
						'echo'					=> 1,
						'hide_empty'			=> 1,
						'hide_title_if_empty'	=> 1,
						'hierarchical'			=> 1,
						'order'					=> 'ASC',
						'orderby' 				=> 'name',
						'separator'				=> '',
						'show_count'			=> 0,
						'style'					=> 'list',
						'title_li'				=> '',
						'taxonomy'				=> 'filter_by',
						'use_desc_for_title'	=> 0,
					);
					wp_list_categories( $args );?>
				</ul>
			</li>
		</ul>


	<?php
	echo '<div class="inline-block vtop menu"><a class="accomfilter btn" href="#accomfilter">+Filter</a></div>';
	$tag_ids = array();

	$i = 0;
	$tag_ids = array();
	while (have_posts()) : the_post();

		if ( 'publish' === get_post_status( $post->ID ) ) {

			$accom_tags = get_the_terms( $post->ID, 'must_have');
			if ($accom_tags) {
				foreach ( $accom_tags as $tag ) {
					$order = get_field('group', $tag);
					if($order == '') { $order = 'zMisc'; };

					$tag_ids[$tag->term_id] = $order . '_ko' . $i;
					++$i;
					unset($order);
				}
			}
		}

	endwhile;
	asort($tag_ids);
	$tag_ids = str_replace('zMisc', 'Misc', $tag_ids );
	$tag_id_only = array_keys($tag_ids);

if ( !empty($tag_ids) ) {

	// Get all tags
	$tags = get_terms( 'must_have' );

	// Get url and convert it to a string
	$url = $_SERVER["REQUEST_URI"];
	$url = (string)$url;

	// Set the string to split the URL by $prefix to find the current tags
	$prefix = "?must_have=";
	$index = strpos($url, $prefix) + strlen($prefix);
	$current_tag = substr($url, $index);

	$url_parts = explode($prefix, $url);
	$url_before_tag = $url_parts[0];
	if (count($url_parts) > 1) {
		$url_after_tag = $url_parts[1];
	} else {
		$url_after_tag = '';
	};

	$current_tags = explode('+', $url_after_tag);
	$url_after_tag = $prefix . $url_after_tag;
	$lastorder = end($tag_ids);
	$lastorder = substr($lastorder, 0, strpos($lastorder, "_ko"));

	if (strpos($url, '?must_have=') !== false) {

		foreach($tag_ids as $tag => $order) {
			$term = get_term( $tag );
			$this_tag = ($term->slug);

			if (in_array($this_tag, $current_tags)) {

				$tag_name = $term->slug;
				$tag_name_plus = '+' . $this_tag;
				$tag_name_plus_after = $this_tag . '+';
				$tag_name_equals = '=' . $this_tag;
				$tag_name_equals_plus = '=' . $this_tag . '+';

				if (strpos($url_after_tag, $tag_name_equals_plus) !== false) {

					// This tag is the first tag in URL and has others following it
					$url_update = str_replace($tag_name_plus_after,"",$url_after_tag);
					$url_update = $url_before_tag . $url_update;

				} else if (strpos($url_after_tag, $tag_name_plus) !== false) {

					// This tag is somewhere in the URL but not the beginning
					$url_update = str_replace($tag_name_plus,"",$url_after_tag);
					$url_update = $url_before_tag . $url_update;

				} else if (strpos($url_after_tag, $tag_name_equals) !== false) {

					// This tag is the first and only tag in URL
					$url_update = $url_before_tag;

				} else if (strpos($url_after_tag, $tag_name_plus) !== false) {

					// This tag is the first tag in the URL and the last
					$url_update = $url_before_tag;

				} else {
					echo "Logic Error";
				}

				echo '<a class="btn tag current" href="'. $url_update . '" rel="tag">'.$term->name.'</a>';

			}

		}

		echo '</div>';

		$orderPrev = '';
		foreach($tag_ids as $tag => $order) {

			$term = get_term( $tag );
			$this_tag = ($term->slug);


			if (!in_array($term->slug, $current_tags)) {

				$order = substr($order, 0, strpos($order, "_ko"));
				if ($orderPrev == '') {?>
					<ul class="inline-block vtop filter menu">
						<li class="menu-item-has-children dropdown">
							<a class="btn" href="#filter"><?php echo $order;?></a>
							<ul class="sub-menu accom">
				<?php } else if ( $orderPrev !== $order) {?>
							</ul>
						</li>
					</ul>
					<ul class="inline-block vtop filter menu">
						<li class="menu-item-has-children dropdown">
							<a class="btn" href="#filter"><?php echo $order;?></a>
								<ul class="sub-menu accom">
				<?php };

				echo '<li><a class="btn tag inactive" href="'.$url.'+' . $term->slug .'" rel="tag">'.$term->name.'</a></li> ';

				$orderPrev = $order;
			}
		}
	echo '</ul></li></ul>';
	} else {

	echo '</div>';

		$orderPrev = '';
		foreach($tag_ids as $tag => $order){

			$order = substr($order, 0, strpos($order, "_ko"));

			if ($orderPrev == '') {?>
				<ul class="inline-block vtop filter menu">
					<li class="menu-item-has-children dropdown">
						<a class="btn" href="#filter"><?php echo $order;?></a>
						<ul class="sub-menu accom">
			<?php } else if ( $orderPrev !== $order ) {?>
						</ul>
					</li>
				</ul>
				<ul class="inline-block vtop filter menu">
					<li class="menu-item-has-children dropdown">
						<a class="btn" href="#filter"><?php echo $order;?></a>
						<ul class="sub-menu accom">
			<?php };

			$orderPrev = $order;
			$tag = get_term( $tag );
			echo '<li><a class="btn tag inactive" href="'.$url.'?must_have=' . $tag->slug .'" rel="tag">'.$tag->name.'</a></li> ';
		}
		echo '</ul></li></ul>';

	}


	$terms = get_terms( 'must_have' );
	$count = count( $terms );
	if ( $count > 0 ) {
		$i = 0;
		foreach ( $terms as $term ) {
			if (!in_array($term->term_id, $tag_id_only)) {
				++$i;
				if ($i <= 1) { ?>
					<ul class="inline-block vtop filter menu unavailable">
						<li class="menu-item-has-children dropdown">
							<a class="btn" href="#filter"><?php echo 'Unavailable';?></a>
							<ul class="sub-menu accom">
				<?php }; ?>
				<li><a class="btn tag disabled" href="#"><?php echo $term->name;?></a></li>
			<?php }
		}
		echo '</ul></li></ul>';
	}

};?>
</nav>
