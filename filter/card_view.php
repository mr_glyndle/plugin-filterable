<?php // vars
$filter_type = get_field('filter_type');?>

<div class="item ic_<?php echo $grid_item_count; ++$grid_item_count;?>">

	<div class="txt_bg"></div>

	<?php if (has_post_thumbnail()) { ?>

		<a class="thumbnail_link image_container" href="<?php echo get_permalink(); ?>">
			<?php the_post_thumbnail('golden_medium', array('class' => 'lazy')); ?>
		</a>

	<?php } ?>

	<a class="text" href="<?php echo get_permalink(); ?>">

		<div class="text_content">

			<h4><?php the_title(); ?></h4>

			<p class="small"><?php echo excerpt(55); ?></p>

			<?php

			$filter_tags = get_the_terms( $post->ID, 'must_have');

			if (!empty($filter_tags)) {

				$count = count( $filter_tags );
				$current = 0;
				echo '<p class="small tags">';
				foreach ( $filter_tags as $tag ) {
				    echo $tag->name;
					++$current;
					if ($current < $count) {
						echo ' | ';
					}
				}
				echo '</p>';

			}

			if ( get_post_status() == 'private' ) {
				show_publish_button();
			};?>
			<p class="small link"><span class="btn" href="<?php echo get_permalink(); ?>">More...</span></p>
		</div>
	</a>
</div>
